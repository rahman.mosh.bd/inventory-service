package com.rms.inventoryservice;

import com.rms.inventoryservice.model.Inventory;
import com.rms.inventoryservice.repository.InventoryRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class InventoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner loadData(InventoryRepository inventoryRepository) {
		return args -> {
			Inventory inventory1 = Inventory.builder().skuCode("yamaha_fz_v3").quantityInStock(12).build() ;
			Inventory inventory2 = Inventory.builder().skuCode("mt_stinger_2").quantityInStock(0).build();

			inventoryRepository.save(inventory1) ;
			inventoryRepository.save(inventory2) ;
		} ;
	}

}
