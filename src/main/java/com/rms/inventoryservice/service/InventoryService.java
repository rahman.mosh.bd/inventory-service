package com.rms.inventoryservice.service;

import com.rms.inventoryservice.dto.InventoryResponseDto;
import com.rms.inventoryservice.model.Inventory;
import com.rms.inventoryservice.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Repository
@RequiredArgsConstructor
public class InventoryService {

    private final InventoryRepository inventoryRepository ;

    @Transactional(readOnly = true)
    public List<InventoryResponseDto> isInStock(List<String> skuCodes) {
        return inventoryRepository
                .findBySkuCodeIn(skuCodes)
                .stream()
                .map(inventory -> InventoryResponseDto
                        .builder()
                        .skuCode(inventory.getSkuCode())
                        .quantityInStock(inventory.getQuantityInStock()>0)
                        .build()
                ).toList() ;
    }
}
